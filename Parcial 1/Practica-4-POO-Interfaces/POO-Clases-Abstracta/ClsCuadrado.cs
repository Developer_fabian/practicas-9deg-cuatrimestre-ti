﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POO_Clases_Abstracta
{
    class ClsCuadrado : ClsFigura
    {
        private float Lado;

        public ClsCuadrado()
        {
            Lado = 0;
        }

        public float Lado1 { get => Lado; set => Lado = value; }

        public float Area()
        {
            return Lado * Lado;
        }

        public float Perimetro()
        {
            return Lado * 4;
        }

        
    }
}
