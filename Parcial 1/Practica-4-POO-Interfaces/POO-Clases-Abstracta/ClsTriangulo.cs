﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POO_Clases_Abstracta
{
    class ClsTriangulo:ClsFigura
    {
        private float Altura;
        private float Base;

        public ClsTriangulo()
        {
            Altura = 0;
            Base = 0;
        }

        public float Altura1 { get => Altura; set => Altura = value; }
        public float Base1 { get => Base; set => Base = value; }

        public float Area()
        {
            return (Altura * Base)/2;
        }

        public float Perimetro()
        {
            return Base*3;
        }
    }
}
