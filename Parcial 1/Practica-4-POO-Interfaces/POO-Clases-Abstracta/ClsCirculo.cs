﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POO_Clases_Abstracta
{
    class ClsCirculo:ClsFigura
    {
        private float radio;
        float pi = 3.1416f;

        public ClsCirculo()
        {
            radio = 0;
        }
        public float Radio { get => radio; set => radio = value; }

        public float Area()
        {
            return pi * (radio * radio);
        }

        public float Perimetro()
        {
            return 2 * pi * radio;
        }
    }
}
