﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POO_calculadora_monomio_x_binomio
{
    class ClsBinomio
    {
        private ClsMonomio monomio1, monomio2;

        public ClsBinomio()
        {
            monomio1 = new ClsMonomio();
            monomio2 = new ClsMonomio();
        }

        public ClsMonomio Monomio1
        {
            get
            {
                return monomio1;
            }
            set
            {
                monomio1 = value;
            }
        }

        public ClsMonomio Monomio2
        {
            get
            {
                return monomio2;
            }
            set
            {
                monomio2 = value;
            }
        }
    }
}
