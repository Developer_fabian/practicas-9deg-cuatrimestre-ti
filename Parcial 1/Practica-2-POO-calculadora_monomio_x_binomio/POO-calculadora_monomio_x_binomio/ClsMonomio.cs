﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POO_calculadora_monomio_x_binomio
{
    class ClsMonomio
    {
        private int coeficiente;
        private string variable;
        private int exponente;

        ClsBinomio res;

        public string Variable      
        {
            get
            {
                return variable;
            }
            set            
            {
                variable = value;
            }
        }

        public int Coeficiente
        {
            get
            {
                return coeficiente;
            }
            set
            {
                coeficiente = value;
            }
        }

        public int Exponente
        {
            get
            {
                return exponente;
            }
            set
            {
                exponente = value;
            }
        }

        public ClsMonomio()
        {
             coeficiente = 0;
             variable = "x";
             exponente = 1;
        }
        int w;
        string final = "";
        public string Multiplicar(ClsBinomio x)
        {
            res = new ClsBinomio();
            res.Monomio1.Coeficiente = x.Monomio1.Coeficiente*this.coeficiente;
            res.Monomio2.Coeficiente = x.Monomio2.Coeficiente * this.coeficiente;

            if (this.variable == x.Monomio1.Variable)
            {
                w = this.exponente + x.Monomio1.Exponente;
                res.Monomio1.Variable = this.variable + "^" + w;
            }
            else
            {
                res.Monomio1.Variable = x.Monomio1.Variable + "^" + x.Monomio1.Exponente + this.Variable + "^" + this.exponente;
            }

            if (this.variable == x.Monomio2.Variable)
            {
                w = this.exponente + x.Monomio2.Exponente;
                res.Monomio2.Variable = this.variable + w;
            }
            else
            {
                res.Monomio2.Variable = x.Monomio2.Variable + "^" + x.Monomio2.Exponente + this.Variable + "^" + this.exponente;
            }
            return final = res.Monomio1.Coeficiente + res.Monomio1.Variable + " " + res.Monomio2.Coeficiente + res.Monomio2.Variable;
        }
    }
}
