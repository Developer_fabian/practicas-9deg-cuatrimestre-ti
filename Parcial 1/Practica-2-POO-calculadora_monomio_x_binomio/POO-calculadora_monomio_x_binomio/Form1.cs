﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POO_calculadora_monomio_x_binomio
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        ClsMonomio monomio = new ClsMonomio();
        ClsBinomio binomio = new ClsBinomio();

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                //monomio 1
                monomio.Coeficiente = int.Parse(txtcoef1.Text);
                monomio.Variable = txtVariable1.Text;
                monomio.Exponente = int.Parse(txtExponente1.Text);

                //monomio 2
                binomio.Monomio1.Coeficiente = int.Parse(txtcoef2.Text);
                binomio.Monomio1.Variable = txtVariable2.Text;
                binomio.Monomio1.Exponente = int.Parse(txtExponente2.Text);

                //monomio 3
                binomio.Monomio2.Coeficiente = int.Parse(txtcoef3.Text);
                binomio.Monomio2.Variable = txtVariable3.Text;
                binomio.Monomio2.Exponente = int.Parse(txtExponente3.Text);

                label4.Text=monomio.Multiplicar(binomio);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
