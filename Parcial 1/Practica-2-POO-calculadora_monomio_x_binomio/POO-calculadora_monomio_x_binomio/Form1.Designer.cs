﻿namespace POO_calculadora_monomio_x_binomio
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalcular = new System.Windows.Forms.Button();
            this.txtcoef1 = new System.Windows.Forms.TextBox();
            this.txtVariable1 = new System.Windows.Forms.TextBox();
            this.txtExponente1 = new System.Windows.Forms.TextBox();
            this.txtExponente2 = new System.Windows.Forms.TextBox();
            this.txtVariable2 = new System.Windows.Forms.TextBox();
            this.txtcoef2 = new System.Windows.Forms.TextBox();
            this.txtExponente3 = new System.Windows.Forms.TextBox();
            this.txtVariable3 = new System.Windows.Forms.TextBox();
            this.txtcoef3 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblresultado = new System.Windows.Forms.Label();
            this.lblmensaje = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(172, 107);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(6);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(98, 51);
            this.btnCalcular.TabIndex = 0;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // txtcoef1
            // 
            this.txtcoef1.Location = new System.Drawing.Point(15, 33);
            this.txtcoef1.Margin = new System.Windows.Forms.Padding(6);
            this.txtcoef1.Name = "txtcoef1";
            this.txtcoef1.Size = new System.Drawing.Size(54, 29);
            this.txtcoef1.TabIndex = 1;
            // 
            // txtVariable1
            // 
            this.txtVariable1.Location = new System.Drawing.Point(78, 33);
            this.txtVariable1.Name = "txtVariable1";
            this.txtVariable1.Size = new System.Drawing.Size(38, 29);
            this.txtVariable1.TabIndex = 2;
            // 
            // txtExponente1
            // 
            this.txtExponente1.Location = new System.Drawing.Point(118, 12);
            this.txtExponente1.Name = "txtExponente1";
            this.txtExponente1.Size = new System.Drawing.Size(23, 29);
            this.txtExponente1.TabIndex = 3;
            // 
            // txtExponente2
            // 
            this.txtExponente2.Location = new System.Drawing.Point(276, 12);
            this.txtExponente2.Name = "txtExponente2";
            this.txtExponente2.Size = new System.Drawing.Size(23, 29);
            this.txtExponente2.TabIndex = 6;
            // 
            // txtVariable2
            // 
            this.txtVariable2.Location = new System.Drawing.Point(235, 33);
            this.txtVariable2.Name = "txtVariable2";
            this.txtVariable2.Size = new System.Drawing.Size(38, 29);
            this.txtVariable2.TabIndex = 5;
            // 
            // txtcoef2
            // 
            this.txtcoef2.Location = new System.Drawing.Point(172, 33);
            this.txtcoef2.Margin = new System.Windows.Forms.Padding(6);
            this.txtcoef2.Name = "txtcoef2";
            this.txtcoef2.Size = new System.Drawing.Size(54, 29);
            this.txtcoef2.TabIndex = 4;
            // 
            // txtExponente3
            // 
            this.txtExponente3.Location = new System.Drawing.Point(416, 12);
            this.txtExponente3.Name = "txtExponente3";
            this.txtExponente3.Size = new System.Drawing.Size(23, 29);
            this.txtExponente3.TabIndex = 9;
            // 
            // txtVariable3
            // 
            this.txtVariable3.Location = new System.Drawing.Point(372, 33);
            this.txtVariable3.Name = "txtVariable3";
            this.txtVariable3.Size = new System.Drawing.Size(38, 29);
            this.txtVariable3.TabIndex = 8;
            // 
            // txtcoef3
            // 
            this.txtcoef3.Location = new System.Drawing.Point(309, 33);
            this.txtcoef3.Margin = new System.Windows.Forms.Padding(6);
            this.txtcoef3.Name = "txtcoef3";
            this.txtcoef3.Size = new System.Drawing.Size(54, 29);
            this.txtcoef3.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(144, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 36);
            this.label1.TabIndex = 10;
            this.label1.Text = "(";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(440, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 36);
            this.label2.TabIndex = 11;
            this.label2.Text = ")";
            // 
            // lblresultado
            // 
            this.lblresultado.AutoSize = true;
            this.lblresultado.Location = new System.Drawing.Point(17, 213);
            this.lblresultado.Name = "lblresultado";
            this.lblresultado.Size = new System.Drawing.Size(99, 24);
            this.lblresultado.TabIndex = 12;
            this.lblresultado.Text = "Resultado:";
            // 
            // lblmensaje
            // 
            this.lblmensaje.AutoSize = true;
            this.lblmensaje.Location = new System.Drawing.Point(122, 213);
            this.lblmensaje.Name = "lblmensaje";
            this.lblmensaje.Size = new System.Drawing.Size(0, 24);
            this.lblmensaje.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(37, 248);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(246, 15);
            this.label3.TabIndex = 14;
            this.label3.Text = "El orden de los valores no altera el producto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(114, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 24);
            this.label4.TabIndex = 15;
            this.label4.Text = "label4";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 272);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblmensaje);
            this.Controls.Add(this.lblresultado);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtExponente3);
            this.Controls.Add(this.txtVariable3);
            this.Controls.Add(this.txtcoef3);
            this.Controls.Add(this.txtExponente2);
            this.Controls.Add(this.txtVariable2);
            this.Controls.Add(this.txtcoef2);
            this.Controls.Add(this.txtExponente1);
            this.Controls.Add(this.txtVariable1);
            this.Controls.Add(this.txtcoef1);
            this.Controls.Add(this.btnCalcular);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Multiplicación";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.TextBox txtcoef1;
        private System.Windows.Forms.TextBox txtVariable1;
        private System.Windows.Forms.TextBox txtExponente1;
        private System.Windows.Forms.TextBox txtExponente2;
        private System.Windows.Forms.TextBox txtVariable2;
        private System.Windows.Forms.TextBox txtcoef2;
        private System.Windows.Forms.TextBox txtExponente3;
        private System.Windows.Forms.TextBox txtVariable3;
        private System.Windows.Forms.TextBox txtcoef3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblresultado;
        private System.Windows.Forms.Label lblmensaje;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

