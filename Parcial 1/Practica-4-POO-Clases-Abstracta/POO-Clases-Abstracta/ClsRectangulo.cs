﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POO_Clases_Abstracta
{
    class ClsRectangulo:ClsFigura
    {
        private float Altura;
        private float Base;
        public ClsRectangulo()
        {
            Altura = 0;
            Base = 0;
        }

        public float Altura1 { get => Altura; set => Altura = value; }
        public float Base1 { get => Base; set => Base = value; }

        public override float Area()
        {
            return Altura * Base;
        }

        public override float Perimetro()
        {
            return Altura + Altura + Base + Base;
        }
    }
}
