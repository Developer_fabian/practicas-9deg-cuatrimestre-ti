﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POO_Clases_Abstracta
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        ClsCirculo Circulo = new ClsCirculo();
        ClsCuadrado Cuadrado = new ClsCuadrado();
        ClsRectangulo Rectangulo = new ClsRectangulo();
        ClsTriangulo Triangulo = new ClsTriangulo();
        ClsRombo Rombo = new ClsRombo();

        ClsFigura y;
        private void btnCalcular1_Click(object sender, EventArgs e)
        {
            Circulo.Radio = float.Parse(txtRadio.Text);
            lblArea1.Text = Circulo.Area().ToString();
            lblPerimetro1.Text = Circulo.Perimetro().ToString();
        }
        int bandera = 0;
        private void btnCalcular2_Click(object sender, EventArgs e)
        {
            switch (bandera)
            {
                case 0://Cuadrado
                    
                    Cuadrado.Lado1 = float.Parse(txtBase.Text);
                    lblArea2.Text = Cuadrado.Area().ToString();
                    lblPerimetro2.Text = Cuadrado.Perimetro().ToString();
                    break;
                case 1: //Rectangulo
                    Rectangulo.Base1 = float.Parse(txtBase.Text);
                    Rectangulo.Altura1 = float.Parse(txtAltura.Text);
                    lblArea2.Text = Rectangulo.Area().ToString();
                    lblPerimetro2.Text = Rectangulo.Perimetro().ToString();
                    break;
                case 2: //Triangulo
                    Triangulo.Base1 = float.Parse(txtBase.Text);
                    Triangulo.Altura1 = float.Parse(txtAltura.Text);
                    lblArea2.Text = Triangulo.Area().ToString();
                    lblPerimetro2.Text = Triangulo.Perimetro().ToString();
                    break;
                case 3://Rombo
                    Rombo.Base1 = float.Parse(txtBase.Text);
                    Rombo.Diametro_mayor1 = float.Parse(txtD_mayor.Text);
                    Rombo.Diametro_menor1 = float.Parse(txtD_menor.Text);
                    lblArea2.Text = Rombo.Area().ToString();
                    lblPerimetro2.Text = Rombo.Perimetro().ToString();
                    break;
                default:
                    break;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            bandera = comboBox1.SelectedIndex;
            string var = comboBox1.Text;
            switch (bandera)
            {

                case 0://Cuadrado
                    limpiar();
                    txtAltura.Enabled = false;
                    txtD_mayor.Enabled = false;
                    txtD_menor.Enabled = false;
                    break;
                case 1: //Rectangulo
                    txtD_mayor.Enabled = false;
                    txtD_menor.Enabled = false;
                    txtAltura.Enabled = true;
                    break;
                case 2: //Triangulo
                    txtD_mayor.Enabled = false;
                    txtD_menor.Enabled = false;
                    txtAltura.Enabled = true;
                    break;
                case 3://Rombo
                    limpiar();
                    txtAltura.Enabled = false;
                    txtD_mayor.Enabled = true;
                    txtD_menor.Enabled = true;
                    break;
                default:
                    limpiar();
                    txtAltura.Enabled = true;
                    txtAltura.Enabled = true;
                    break;
            }
        }
        private void limpiar()
        {
            txtAltura.Text = "";
            txtBase.Text = "";
            txtD_mayor.Text = "";
            txtD_menor.Text = "";
            txtRadio.Text="";
        }
    }
}
