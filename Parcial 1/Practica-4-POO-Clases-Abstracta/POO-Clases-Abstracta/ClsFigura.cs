﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POO_Clases_Abstracta
{
    abstract class ClsFigura
    {
        public abstract float Area();
        public abstract float Perimetro();

        /////////////////////
        public string Valores()
        {
            return "area:" + Area()+" Prerimetro:"+Perimetro();
        }


    }
}
