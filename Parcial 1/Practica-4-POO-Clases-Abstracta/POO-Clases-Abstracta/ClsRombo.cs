﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POO_Clases_Abstracta
{
    class ClsRombo:ClsFigura
    {
        private float Diametro_mayor;
        private float Diametro_menor;
        private float Base;

        public ClsRombo()
        {
            Diametro_mayor1 = 0;
            Diametro_menor1 = 0;
            Base = 0;
        }

        public float Diametro_mayor1 { get => Diametro_mayor; set => Diametro_mayor = value; }
        public float Diametro_menor1 { get => Diametro_menor; set => Diametro_menor = value; }
        public float Base1 { get => Base; set => Base = value; }

        public override float Area()
        {
            return (Diametro_mayor * Diametro_menor1)/2;
        }

        public override float Perimetro()
        {
            return Base*4;
        }
    }
}
