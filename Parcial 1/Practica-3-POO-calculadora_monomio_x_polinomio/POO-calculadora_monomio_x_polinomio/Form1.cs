﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POO_calculadora_monomio_x_polinomio
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        ClsMonomio monomio = new ClsMonomio();
        ClsPolinomio polinomio = new ClsPolinomio();
        string mono = "";
        string poli = ""; 
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (radioMonomio.Checked)
                {
                    //pasar valores 
                    monomio.Coeficiente = int.Parse(txtCoeficiente.Text);
                    monomio.Variable = txtVariable.Text;
                    monomio.Exponente = int.Parse(txtExponente.Text);
                    
                    radioPolinomio.Checked = false;
                    
                    mono ="("+int.Parse(txtCoeficiente.Text)+ txtVariable.Text+"^"+int.Parse(txtExponente.Text)+")";
                    //limpia las cajas de texto
                    limpiar();
                }
                else if (radioPolinomio.Checked)
                {
                    //pasar valores
                    polinomio.Monomio1.Coeficiente = int.Parse(txtCoeficiente.Text);
                    polinomio.Monomio1.Variable = txtVariable.Text;
                    polinomio.Monomio1.Exponente = int.Parse(txtExponente.Text);

                    //realiza la multiplicacion 
                    monomio.Multiplicar(polinomio);

                    poli =poli+int.Parse(txtCoeficiente.Text)+ txtVariable.Text+"^"+int.Parse(txtExponente.Text);
                    radioMonomio.Checked = false;
                    //limpia las cajas de texto
                    limpiar();
                }

                //almacen conforme se agregan los valores y los muestra
                label4.Text = mono+"("+poli+")";


                radioPolinomio.Checked = false;
                radioMonomio.Checked = false;
                radioMonomio.Enabled = false;
            }
            catch (Exception)
            {

                throw;
            }   
        }
        public void limpiar()
        {
            txtCoeficiente.Text = "";
            txtExponente.Text = "";
            txtVariable.Text = "";
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            label1.Text = monomio.Resultado();
        }
    }
}
