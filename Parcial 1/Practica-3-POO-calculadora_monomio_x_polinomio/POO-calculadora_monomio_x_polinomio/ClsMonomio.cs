﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POO_calculadora_monomio_x_polinomio
{
    class ClsMonomio
    {
        private int coeficiente;
        private string variable;
        private int exponente;
        ClsPolinomio res;
    
        public int Coeficiente { get => coeficiente; set => coeficiente = value; }
        public string Variable { get => variable; set => variable = value; }
        public int Exponente { get => exponente; set => exponente = value; }

        string final = "";
        string cadena;
        string coe;
        string var;
        int w;                                  
        public void Multiplicar(ClsPolinomio x)
        {
            res = new ClsPolinomio();
            res.Monomio1.Coeficiente = x.Monomio1.Coeficiente * this.coeficiente;

            coe = res.Monomio1.Coeficiente.ToString();//coeficiente

            if (this.variable == x.Monomio1.Variable)
            {
                w = this.exponente + x.Monomio1.Exponente;
                res.Monomio1.Variable = this.variable + "^" + w;
                var = res.Monomio1.Variable;//variable
            }
            else
            {
                res.Monomio1.Variable = x.Monomio1.Variable + "^" + x.Monomio1.Exponente + this.Variable + "^" + this.exponente;
                var = res.Monomio1.Variable;//variable
            }
            cadena = coe + var;
            final = final + cadena;
        }

        public string Resultado()
        {
            return final;
        }
    }
}
