﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POO_calculadora_monomio_x_polinomio
{
    class ClsPolinomio
    {
        private ClsMonomio monomio1;

        public ClsPolinomio()
        {
            monomio1 = new ClsMonomio();
        }
        public ClsMonomio Monomio1 { get => monomio1; set => monomio1 = value; }
    }
}
