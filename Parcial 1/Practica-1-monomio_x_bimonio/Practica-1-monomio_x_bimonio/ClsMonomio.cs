﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_1_monomio_x_bimonio
{
    public class ClsMonomio
    {
        private string signo;
        private int coeficiente;
        private string variable;
        private int exponente;
        
        public string Signo
        {
            get                //lectura
            {
                return signo;
            }
            set                //escritura
            {

                signo=value;
            }
        }
        public int Coeficiente
        {
            get                
            {
                return coeficiente;
            }
            set                
            {
                coeficiente = value;
            }
        }
        public string Variable
        {
            get                
            {
                return variable;
            }
            set                
            {
                variable = value;
            }
        }
        public int Exponente
        {
            get                
            {
                return exponente;
            }
            set                
            {
                exponente = value;
            }
        }
    }
}
