﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_1_monomio_x_bimonio
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        ClsMonomio monomio = new ClsMonomio();
        ClsBinomio binomio = new ClsBinomio();
        private void btnCalcular_Click(object sender, EventArgs e)
        {
            //1er signo
            if (txtsigno1_m.Text == "")
            {
                monomio.Signo = "+";
            }
            else
            {
                monomio.Signo = txtsigno1_m.Text;
            }
            
            binomio.Signo_b = txtsigno1_b.Text;
            
            //coeficiente
            monomio.Coeficiente = int.Parse(txtCoeficiente_m.Text);
            binomio.Coeficiente_b = int.Parse(txtcoeficiente_b.Text);
            //variable
            monomio.Variable = txtVariable_m.Text;
            binomio.Variable_b = txtvariable_b.Text;

            //exponente
            //monomio.Exponente = int.Parse(txtexponente_m.Text);
            if (txtexponente_m.Text == "")
            {
                monomio.Exponente = 1;
            }
            else
            {
                monomio.Exponente = int.Parse(txtexponente_m.Text);
            }

            //binomio.Exponente_b = int.Parse(txtexponente_b.Text);
            if (txtexponente_b.Text == "")
            {
                binomio.Exponente_b = 1;
            }
            else
            {
                binomio.Exponente_b = int.Parse(txtexponente_b.Text);
            }
            //signo1
            binomio.Signo1_b = txtsigno2_b.Text;
            //coeficiente1
            binomio.Coeficiente1_b = int.Parse(txtcoeficiente1_b.Text);
            //variable1
            binomio.Variable1_b = txtvariable1_b.Text;
            //exponente1
            // binomio.Exponente1_b=int.Parse(txtexponente1_b.Text);
            if (txtexponente1_b.Text == "")
            {
                binomio.Exponente1_b = 1;
            }
            else
            {
                binomio.Exponente1_b = int.Parse(txtexponente1_b.Text);
            }
            //Mensaje
            label4.Text=(binomio.Multiplicar(monomio.Signo, monomio.Coeficiente, monomio.Variable,monomio.Exponente));
        }

        private void txtVariable_m_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Solo letras");
            }
        }

        private void txtvariable_b_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Solo letras");
            }
        }

        private void txtvariable1_b_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Solo letras");
            }
        }

        private void txtvariable2_b_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Solo letras");
            }
        }

        private void txtCoeficiente_m_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Solo números");
            }
        }

        private void txtcoeficiente_b_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Solo números");
            }
        }

        private void txtcoeficiente1_b_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Solo números");
            }
        }

        private void txtexponente_m_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Solo números");
            }
        }

        private void txtexponente_b_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Solo números");
            }
        }

        private void txtexponente1_b_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Solo números");
            }
        }
    }
}
