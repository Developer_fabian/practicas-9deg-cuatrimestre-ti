﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_1_monomio_x_bimonio
{
    public class ClsBinomio
    {
        private string signo_b;
        private int coeficiente_b;
        private string variable_b;
        private int exponente_b;

        private string signo1_b;
        private int coeficiente1_b;
        private string variable1_b;
        private int exponente1_b;

        public string Signo_b { get => signo_b; set => signo_b = value; }
        public int Coeficiente_b { get => coeficiente_b; set => coeficiente_b = value; }
        public string Variable_b { get => variable_b; set => variable_b = value; }
        public int Exponente_b { get => exponente_b; set => exponente_b = value; }
        public string Signo1_b { get => signo1_b; set => signo1_b = value; }
        public int Coeficiente1_b { get => coeficiente1_b; set => coeficiente1_b = value; }
        public string Variable1_b { get => variable1_b; set => variable1_b = value; }
        public int Exponente1_b { get => exponente1_b; set => exponente1_b = value; }

        string[] Resultado = new string[7];
        string final;

        public string Multiplicar(string signo_monomio, int coeficiente_monomio, string variable_monomio, int exponente_monomio)//, , 
        {
            string Signo_ = Signo(signo_monomio, signo_b);//signo
            if (Signo_ == "+")
            {
                Resultado[0] = ""; 
            }
            else
            {
                Resultado[0] = Signo_;
            }
            Resultado[1] = (coeficiente_monomio * coeficiente_b).ToString();//coeficiente
            Resultado[2] = Variable(variable_monomio, variable_b,exponente_monomio,exponente_b);//variable
            Resultado[3] = Signo(signo_monomio,signo1_b);//signo1
            Resultado[4] = (coeficiente_monomio * Coeficiente1_b).ToString();
            Resultado[5] = Variable(variable_monomio, Variable1_b, exponente_monomio, exponente1_b);
            final = Resultado[0] + Resultado[1]+Resultado[2]+Resultado[3]+Resultado[4]+Resultado[5];
            
            return final;
        }

        public string Signo(string signo1, string signo2)
        {
            string Signofinal = "";
            if (signo1 == "+" && signo2 == "+")
            {
                Signofinal = "+";
            }
            else if (signo1 == "+" && signo2 == "-")
            {
                Signofinal = "-";
            }
            else if (signo1 == "-" && signo2 == "+")
            {
                Signofinal = "-";
            }
            else if (signo1 == "-" && signo2 == "-")
            {
                Signofinal = "+";
            }
            return Signofinal;
        }

        string cadena;
        int exponente;
        public string Variable(string variable1, string variable2, int exponente1, int exponente2)
        {
            cadena = "";
            exponente = 0;
            if (variable1 == variable2)
            {    
                exponente = exponente1 + exponente2;
                if (exponente != 0)
                {
                    if (exponente == 1)
                    {
                        cadena = variable1;
                    }
                    else
                    {
                        cadena = variable1 + "^" + exponente;
                    }
                }
                //cadena = variable1+"^"+exponente;
            }
            else if (variable1 != variable2)
            {
                if (exponente1 != 0)
                {
                    if (exponente1 == 1)
                    {
                        cadena = cadena + variable1;
                    }
                    else
                    {
                        cadena = cadena + variable1 + "^" + exponente1;
                    }    
                }
                if (exponente2 != 0)
                {
                    if (exponente2 == 1)
                    {
                        cadena = cadena + variable2;
                    }
                    else
                    {
                        cadena = cadena + variable2 + "^" + exponente2;
                    }
                }

                //cadena = variable1 + "^" + exponente1 + variable2+"^"+exponente2;
            }
            return cadena;
        }

        //public string Variable(string variable1, string variable2,string variable3, int exponente1, int exponente2, int exponente3)
        //{
        //    cadena = "";
        //    exponente = 0;
        //    if (variable1 == variable2)
        //    {
        //        exponente = exponente1 + exponente2;
        //        cadena = variable1 + "^" + exponente+variable3+"^"+exponente3;
        //    }
        //    else if (variable1 == variable3)
        //    {
        //        exponente = exponente1 + exponente3;
        //        cadena = variable2 + "^" + exponente2 + variable1 + "^" + exponente;
        //    }   
        //    return cadena;
        //}
    }
}
