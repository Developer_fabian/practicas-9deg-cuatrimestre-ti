﻿namespace Practica_1_monomio_x_bimonio
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalcular = new System.Windows.Forms.Button();
            this.txtsigno1_m = new System.Windows.Forms.TextBox();
            this.txtCoeficiente_m = new System.Windows.Forms.TextBox();
            this.txtVariable_m = new System.Windows.Forms.TextBox();
            this.txtexponente_m = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtexponente_b = new System.Windows.Forms.TextBox();
            this.txtvariable_b = new System.Windows.Forms.TextBox();
            this.txtcoeficiente_b = new System.Windows.Forms.TextBox();
            this.txtsigno1_b = new System.Windows.Forms.TextBox();
            this.txtexponente1_b = new System.Windows.Forms.TextBox();
            this.txtvariable1_b = new System.Windows.Forms.TextBox();
            this.txtcoeficiente1_b = new System.Windows.Forms.TextBox();
            this.txtsigno2_b = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(204, 100);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 33);
            this.btnCalcular.TabIndex = 0;
            this.btnCalcular.Text = "Cancular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // txtsigno1_m
            // 
            this.txtsigno1_m.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsigno1_m.Location = new System.Drawing.Point(12, 44);
            this.txtsigno1_m.MaxLength = 1;
            this.txtsigno1_m.Name = "txtsigno1_m";
            this.txtsigno1_m.Size = new System.Drawing.Size(19, 20);
            this.txtsigno1_m.TabIndex = 1;
            this.txtsigno1_m.Text = "+";
            // 
            // txtCoeficiente_m
            // 
            this.txtCoeficiente_m.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCoeficiente_m.Location = new System.Drawing.Point(37, 41);
            this.txtCoeficiente_m.Name = "txtCoeficiente_m";
            this.txtCoeficiente_m.Size = new System.Drawing.Size(34, 29);
            this.txtCoeficiente_m.TabIndex = 2;
            this.txtCoeficiente_m.Text = "2";
            this.txtCoeficiente_m.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCoeficiente_m_KeyPress);
            // 
            // txtVariable_m
            // 
            this.txtVariable_m.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVariable_m.Location = new System.Drawing.Point(77, 41);
            this.txtVariable_m.MaxLength = 1;
            this.txtVariable_m.Name = "txtVariable_m";
            this.txtVariable_m.Size = new System.Drawing.Size(38, 29);
            this.txtVariable_m.TabIndex = 3;
            this.txtVariable_m.Text = "x";
            this.txtVariable_m.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVariable_m_KeyPress);
            // 
            // txtexponente_m
            // 
            this.txtexponente_m.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexponente_m.Location = new System.Drawing.Point(121, 31);
            this.txtexponente_m.Name = "txtexponente_m";
            this.txtexponente_m.Size = new System.Drawing.Size(27, 20);
            this.txtexponente_m.TabIndex = 4;
            this.txtexponente_m.Text = "1";
            this.txtexponente_m.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtexponente_m_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(154, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "(";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(460, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 29);
            this.label2.TabIndex = 6;
            this.label2.Text = ")";
            // 
            // txtexponente_b
            // 
            this.txtexponente_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexponente_b.Location = new System.Drawing.Point(285, 31);
            this.txtexponente_b.Name = "txtexponente_b";
            this.txtexponente_b.Size = new System.Drawing.Size(27, 20);
            this.txtexponente_b.TabIndex = 10;
            this.txtexponente_b.Text = "1";
            this.txtexponente_b.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtexponente_b_KeyPress);
            // 
            // txtvariable_b
            // 
            this.txtvariable_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvariable_b.Location = new System.Drawing.Point(241, 41);
            this.txtvariable_b.MaxLength = 1;
            this.txtvariable_b.Name = "txtvariable_b";
            this.txtvariable_b.Size = new System.Drawing.Size(38, 29);
            this.txtvariable_b.TabIndex = 9;
            this.txtvariable_b.Text = "t";
            this.txtvariable_b.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtvariable_b_KeyPress);
            // 
            // txtcoeficiente_b
            // 
            this.txtcoeficiente_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcoeficiente_b.Location = new System.Drawing.Point(201, 41);
            this.txtcoeficiente_b.Name = "txtcoeficiente_b";
            this.txtcoeficiente_b.Size = new System.Drawing.Size(34, 29);
            this.txtcoeficiente_b.TabIndex = 8;
            this.txtcoeficiente_b.Text = "1";
            this.txtcoeficiente_b.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcoeficiente_b_KeyPress);
            // 
            // txtsigno1_b
            // 
            this.txtsigno1_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsigno1_b.Location = new System.Drawing.Point(176, 44);
            this.txtsigno1_b.MaxLength = 1;
            this.txtsigno1_b.Name = "txtsigno1_b";
            this.txtsigno1_b.Size = new System.Drawing.Size(19, 20);
            this.txtsigno1_b.TabIndex = 7;
            this.txtsigno1_b.Text = "+";
            // 
            // txtexponente1_b
            // 
            this.txtexponente1_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtexponente1_b.Location = new System.Drawing.Point(427, 31);
            this.txtexponente1_b.Name = "txtexponente1_b";
            this.txtexponente1_b.Size = new System.Drawing.Size(27, 20);
            this.txtexponente1_b.TabIndex = 14;
            this.txtexponente1_b.Text = "1";
            this.txtexponente1_b.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtexponente1_b_KeyPress);
            // 
            // txtvariable1_b
            // 
            this.txtvariable1_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvariable1_b.Location = new System.Drawing.Point(383, 41);
            this.txtvariable1_b.MaxLength = 1;
            this.txtvariable1_b.Name = "txtvariable1_b";
            this.txtvariable1_b.Size = new System.Drawing.Size(38, 29);
            this.txtvariable1_b.TabIndex = 13;
            this.txtvariable1_b.Text = "x";
            this.txtvariable1_b.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtvariable1_b_KeyPress);
            // 
            // txtcoeficiente1_b
            // 
            this.txtcoeficiente1_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcoeficiente1_b.Location = new System.Drawing.Point(343, 41);
            this.txtcoeficiente1_b.Name = "txtcoeficiente1_b";
            this.txtcoeficiente1_b.Size = new System.Drawing.Size(34, 29);
            this.txtcoeficiente1_b.TabIndex = 12;
            this.txtcoeficiente1_b.Text = "4";
            this.txtcoeficiente1_b.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcoeficiente1_b_KeyPress);
            // 
            // txtsigno2_b
            // 
            this.txtsigno2_b.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsigno2_b.Location = new System.Drawing.Point(318, 44);
            this.txtsigno2_b.MaxLength = 1;
            this.txtsigno2_b.Name = "txtsigno2_b";
            this.txtsigno2_b.Size = new System.Drawing.Size(19, 20);
            this.txtsigno2_b.TabIndex = 11;
            this.txtsigno2_b.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(33, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 25);
            this.label3.TabIndex = 17;
            this.label3.Text = "Resultado:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(154, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 25);
            this.label4.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 235);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtexponente1_b);
            this.Controls.Add(this.txtvariable1_b);
            this.Controls.Add(this.txtcoeficiente1_b);
            this.Controls.Add(this.txtsigno2_b);
            this.Controls.Add(this.txtexponente_b);
            this.Controls.Add(this.txtvariable_b);
            this.Controls.Add(this.txtcoeficiente_b);
            this.Controls.Add(this.txtsigno1_b);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtexponente_m);
            this.Controls.Add(this.txtVariable_m);
            this.Controls.Add(this.txtCoeficiente_m);
            this.Controls.Add(this.txtsigno1_m);
            this.Controls.Add(this.btnCalcular);
            this.Name = "Form1";
            this.Text = "Multiplicación monomio x binomio";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.TextBox txtsigno1_m;
        private System.Windows.Forms.TextBox txtCoeficiente_m;
        private System.Windows.Forms.TextBox txtVariable_m;
        private System.Windows.Forms.TextBox txtexponente_m;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtexponente_b;
        private System.Windows.Forms.TextBox txtvariable_b;
        private System.Windows.Forms.TextBox txtcoeficiente_b;
        private System.Windows.Forms.TextBox txtsigno1_b;
        private System.Windows.Forms.TextBox txtexponente1_b;
        private System.Windows.Forms.TextBox txtvariable1_b;
        private System.Windows.Forms.TextBox txtcoeficiente1_b;
        private System.Windows.Forms.TextBox txtsigno2_b;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

