﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Perimetro_Area_Figuras
{
    class ClsFigura
    {
        private float altura;
        private float pi = 3.1416f;
        private float radio;
        private float base1;
        private float base2;
        private float base3;
        private float base_menor;
        private float base_mayor;
        //private float resultado;
       

        public float Altura { get => altura; set => altura = value; }
        public float Base1 { get => base1; set => base1 = value; }
        public float Radio { get => radio; set => radio = value; }
        public float Base2 { get => base2; set => base2 = value; }
        public float Base3 { get => base3; set => base3 = value; }
        public float Base_menor { get => base_menor; set => base_menor = value; }
        public float Base_mayor { get => base_mayor; set => base_mayor = value; }
        //****************************************************//
        public float Cuadrado_area()
        {
            return base1 * base1;
        }
        public float Cuadrado_perimetro()
        {
            return base1 + base1 + base1 + base1;
        }
        //****************************************************//
        public float Rectangulo_area()
        {
            return base1 * altura;
        }

        public float Rectangulo_perimetro()
        {
            return base1 + base1 + altura + altura;
        }
        //****************************************************//
        public float Circulo_area()
        {
            return pi * (radio * radio);
        }

        public float Circulo_perimetro()
        {
            return 2 * pi * radio;
        }
        //****************************************************//
        public float Triangulo_area()
        {
            return (base1 * altura) / 2;
        }
        
        public float Triangulo_perimetro()
        {
            return base1 + altura + base2;
        }
        //****************************************************//
        public float Trapecio_area()
        {
            return ((Base_mayor * base_menor) * altura) / 2;
        }
        public float Trapecio_perimetro()
        {
            return base_mayor + Base_mayor + base1 + base2;
        }
        //****************************************************//
    }
}
