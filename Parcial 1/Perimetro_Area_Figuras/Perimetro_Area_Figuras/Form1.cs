﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Perimetro_Area_Figuras
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void cmbFigura_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbFigura.Text == "Cuadrado")
            {
                txt1.Enabled = true;

                txt2.Enabled = false;
                txt3.Enabled = false;
                txt4.Enabled = false;
            }
            else if (cmbFigura.Text == "Rectangulo")
            {
                txt1.Enabled = true;
                txt2.Enabled = true;
                txt3.Enabled = false;
                txt4.Enabled = false;
            }
            else if (cmbFigura.Text == "Circulo")
            {
                txt1.Enabled = false;
                txt2.Enabled = false;
                txt3.Enabled = true;
                txt4.Enabled = false;
            }
            else if (cmbFigura.Text == "Triangulo")
            {
                txt1.Enabled = true;
                txt2.Enabled = true;
                txt3.Enabled = true;
                txt4.Enabled = true;

                
           
            }
            else if (cmbFigura.Text == "Trapecio")
            {

            }

        }

        ClsFigura Objeto = new ClsFigura();
        private void btnCalcular_Click(object sender, EventArgs e)
        {
            
            if (cmbFigura.Text == "Cuadrado")
            {
                Objeto.Base1 = float.Parse(txt1.Text);

                lblArea.Text = Objeto.Cuadrado_area().ToString();
                lblPerimetro.Text = Objeto.Cuadrado_perimetro().ToString();  
            }
            else if (cmbFigura.Text == "Rectangulo")
            {
                Objeto.Base1 = float.Parse(txt1.Text);
                Objeto.Altura = float.Parse(txt2.Text);

                lblArea.Text = Objeto.Rectangulo_area().ToString();
                lblPerimetro.Text = Objeto.Rectangulo_perimetro().ToString();
            }
            else if (cmbFigura.Text == "Circulo")
            {
                Objeto.Radio = float.Parse(txt3.Text);
                lblArea.Text = Objeto.Circulo_area().ToString();
                lblPerimetro.Text = Objeto.Circulo_perimetro().ToString();
            }
            else if (cmbFigura.Text == "Triangulo")
            {
                Objeto.Base1 = float.Parse(txt1.Text);
                Objeto.Altura = float.Parse(txt2.Text);
                Objeto.Base2 = float.Parse(txt3.Text);

                lblArea.Text = Objeto.Triangulo_area().ToString();
                lblPerimetro.Text = Objeto.Triangulo_perimetro().ToString();
            }
            else if (cmbFigura.Text == "Trapecio")
            {
              
            }
        }

        public void invisible()
        {
            lbl1.Enabled = false;
            lbl2.Enabled = false;
            lbl3.Enabled = false;
            lbl4.Enabled = false;
            txt1.Enabled = false;
            txt2.Enabled = false;
            txt3.Enabled = false;
            txt4.Enabled = false;

            txt1.Text = "";
            txt2.Text = "";
            txt3.Text = "";
            txt4.Text = "";
        }
    

        private void Form1_Load(object sender, EventArgs e)
        {
            txt1.Enabled = false;
            txt2.Enabled = false;
            txt3.Enabled = false;
            txt4.Enabled = false;
        }
    }
}
