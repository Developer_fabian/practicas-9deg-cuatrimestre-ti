﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploMVC.Modelo.VO
{
    class VOUsuario
    {
        private string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        string Nombre;

        public string Nombre1
        {
            get { return Nombre; }
            set { Nombre = value; }
        }
        int Edad;

        public int Edad1
        {
            get { return Edad; }
            set { Edad = value; }
        }
        string Direccion;

        public string Direccion1
        {
            get { return Direccion; }
            set { Direccion = value; }
        }
    }
}
