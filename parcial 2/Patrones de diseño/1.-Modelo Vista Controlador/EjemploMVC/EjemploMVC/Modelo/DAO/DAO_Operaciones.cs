﻿using EjemploMVC.Modelo.VO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Drawing;

namespace EjemploMVC.Modelo.DAO
{
    class DAO_Operaciones:Cls_Conexion
    {
        

        ////Aqui tienen que ir todos los metodos
        MySqlCommand agregar;
        MySqlCommand modificar;
        MySqlCommand eliminar;
        DataTable tabla = new DataTable();
        MySqlDataAdapter consultar;
        public DataTable Consulta()
        {
            conexion();
            tabla = new DataTable();
            string cadenaconsulta = "SELECT IdUsuario, Nombre, Edad, Direccion FROM usuario";
            consultar = new MySqlDataAdapter(cadenaconsulta, conectar);
            consultar.Fill(tabla);
            return tabla;
        }
        ///Metodo para Agregar
        public void Agregar(VOUsuario Vo)
        {
            conexion();
            try
            {
                string Cadenausuario = "INSERT INTO usuario (IdUsuario, Nombre, Edad, Direccion) VALUES ('"+Vo.Id +"', '"+Vo.Nombre1+"',"+Vo.Edad1+",'"+Vo.Direccion1+"') ";
                agregar = new MySqlCommand(Cadenausuario, conectar);
                agregar.ExecuteNonQuery();
                System.Windows.Forms.MessageBox.Show("Datos guardados", "Aviso", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

            }
            catch(Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Error al insertar los datos..."+e.Message, "Aviso", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }
        //////////////////////////Metodo para Eliminar
        public void Eliminar(VOUsuario Vo)
        {
            conexion();
            try
            {
                //se coloca la sentencia que va en sql para realizar dicha operación 
                string cadenaempleado = "DELETE from usuario Where IdUsuario ='"+ Vo.Id +"'";
                eliminar = new MySqlCommand(cadenaempleado, conectar);
                eliminar.ExecuteNonQuery();
                System.Windows.Forms.MessageBox.Show("Datos eliminados", "Aviso", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }
            catch(Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message+ "Error al eliminar los datos...", "Aviso", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }
        ////////////////////Metodo para Modificar
        public void Actualizar(VOUsuario Vo)
        {
            try
            {
                conexion();
                string cadenausuaeio = "UPDATE usuario SET Nombre='"+Vo.Nombre1+"', Edad="+Vo.Edad1+", Direccion='"+Vo.Direccion1+"' WHERE IdUsuario='"+Vo.Id+"'";
                modificar = new MySqlCommand(cadenausuaeio, conectar);
                modificar.ExecuteNonQuery();

                System.Windows.Forms.MessageBox.Show("Datos actualizados...", "Aviso", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Error al actualizar los datos...", "Aviso", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        //////////////////////////////////
        ///////////VALIDACIONES
        public bool resp1, resp2;
        public ErrorProvider errores = new ErrorProvider();

        ///VALIDACION DE NUMEROS
        public void SoloNum(KeyPressEventArgs E, TextBox caja)
        {

            if (char.IsNumber(E.KeyChar))
            {
                errores.Clear();
                E.Handled = false;
            }
            else
            {
                if (char.IsControl(E.KeyChar))
                {
                    errores.Clear();
                    E.Handled = false;
                }
                else
                {
                    if (char.IsWhiteSpace(E.KeyChar))
                    {
                        E.Handled = true;
                        errores.SetError(caja, "No se aceptan espacios en blanco");
                    }
                    else
                    {
                        E.Handled = true;
                        errores.SetError(caja, "Se aceptan solo números");
                    }
                }

            }
        }
        ////////////////////////VALIDACION DE NUMEROS, LETRAS Y ESPACIOS.
        public void NumyLetraEspacio(KeyPressEventArgs E, TextBox caja)
        {
            if (char.IsNumber(E.KeyChar))
            {
                errores.Clear();
                E.Handled = false;
            }
            else
            {
                if (char.IsControl(E.KeyChar))
                {
                    errores.Clear();
                    E.Handled = false;
                }
                else
                {
                    E.Handled = false;
                    errores.Clear();
                }
            }
        }
        //////////////VALIDACION DE SOLO LETRAS
        internal void SoloLetras(KeyPressEventArgs E, TextBox caja)
        {
            if (char.IsNumber(E.KeyChar))
            {
                errores.SetError(caja, "No se aceptan números");
                E.Handled = true;
            }
            else
            {
                if (char.IsControl(E.KeyChar))
                {
                    errores.Clear();
                    E.Handled = false;

                }
                else
                {
                    if (char.IsWhiteSpace(E.KeyChar))
                    {
                        E.Handled = false;
                        errores.SetError(caja, string.Empty);
                    }
                    else
                    {
                        E.Handled = false;
                        errores.SetError(caja, string.Empty);
                    }
                }
            }
        }
        //////////VALIDACION DE CAJAS
        public bool Cajas(Form todo)
        {
            resp1 = false;
            foreach (Control controles in todo.Controls)
            {
                if (controles is TextBox)
                {
                    if (controles.Text == string.Empty)
                    {
                        errores.SetError(controles, "Campo obligatorio");
                        resp1 = true;
                    }
                    else
                    {
                        errores.SetError(controles, string.Empty);
                    }
                }
            }
            return resp1;
        }
        /////////VALIDACION DE LIMPIAR FORMULARIOS
        public void Limpiar(Form formulario)
        {
            foreach (Control controles in formulario.Controls)
            {
                if (controles is TextBox)
                {
                    controles.Text = string.Empty;
                }
                else
                {
                    if (controles is ComboBox)
                    {
                        controles.Text = string.Empty;
                    }
                }
            }
        }
        ///////////////
    }
}
