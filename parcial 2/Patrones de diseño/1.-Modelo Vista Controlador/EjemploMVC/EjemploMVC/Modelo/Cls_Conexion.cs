﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;

namespace EjemploMVC.Modelo
{
    class Cls_Conexion
    {
        public MySqlConnection conectar;
        private string servidor;

        public string Servidor
        {
            get { return servidor; }
            set { servidor = value; }
        }
        private string bd;

        public string Bd
        {
            get { return bd; }
            set { bd = value; }
        }
        private string user;

        public string User
        {
            get { return user; }
            set { user = value; }
        }
        private string pass;

        public string Pass
        {
            get { return pass; }
            set { pass = value; }
        }
        private string cadena;

        public string Cadena
        {
            get { return cadena; }
            set { cadena = value; }
        }
        public void conexion()
        {
            try
            {
                cadena = "server=localhost; Database = ejemplomvc; User Id=root;";
                conectar = new MySqlConnection(cadena);
                conectar.Open();
                
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("Error en la conexión con la base de datos...");
            }
        }
    }
}
