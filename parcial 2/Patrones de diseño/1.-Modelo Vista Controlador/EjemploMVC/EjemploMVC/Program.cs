﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using EjemploMVC.Controlador;

namespace EjemploMVC
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        /// 
        
        [STAThread]
        static void Main()
        {
            
            Application.EnableVisualStyles();
            ////Application.SetCompatibleTextRenderingDefault(false);
            ///Application.Run(new Form1());
            Controlador.Controlador con = new Controlador.Controlador();
            con.Eventos();
        }
    }
}
