﻿using EjemploMVC.Modelo.DAO;
using EjemploMVC.Modelo.VO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjemploMVC.Controlador
{
    class Controlador
    {
        Vista.Form_usu x;
        DAO_Operaciones opera = new DAO_Operaciones();
        VOUsuario usuarios;
        
        ///////////Eventos
        public void Eventos()
        {
            x = new Vista.Form_usu();
            usuarios = new VOUsuario();
            x.btn_agregar.Click+= btn_agregar_Click;
            x.btn_Modificar.Click+=btn_Modificar_Click;
            x.btn_Eliminar.Click+=btn_Eliminar_Click;
            x.btn_limpiar.Click+=btn_limpiar_Click;
            x.txt_Id.KeyPress+=txt_Id_KeyPress;
            x.txt_Nombre.KeyPress+=txt_Nombre_KeyPress;
            x.txt_Edad.KeyPress+=txt_Edad_KeyPress;
            x.txt_Direccion.KeyPress+=txt_Direccion_KeyPress;
            x.Dgv_usuarios.CellClick+=Dgv_usuarios_CellClick;
           Application.EnableVisualStyles();
           x.ShowDialog();
        }
        //Boton Limpiar
        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            x.txt_Id.ReadOnly = false;
            x.txt_Id.Clear();
            x.txt_Nombre.Clear();
            x.txt_Edad.Clear();
            x.txt_Direccion.Clear();
        }

        //Boton Eliminar
        private void btn_Eliminar_Click(object sender, EventArgs e)
        {
            if (x.btn_Eliminar.Text == "Eliminar")
            {
                opera.Cajas(x);
                if (x.txt_Id.Text != "")
                {
                    usuarios.Id = x.txt_Id.Text;
                    usuarios.Nombre1 = x.txt_Nombre.Text;
                    x.txt_Edad.Text = Convert.ToString(usuarios.Edad1);
                    usuarios.Direccion1 = x.txt_Direccion.Text;
                   
                    opera.Eliminar(usuarios);
                    x.Dgv_usuarios.DataSource = opera.Consulta();
                    opera.Limpiar(x);

                }
            }
            else
            {
                MessageBox.Show("Hay datos vacios", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        ///Boton Modificar
        private void btn_Modificar_Click(object sender, EventArgs e)
        {
            if (x.btn_Modificar.Text == "Actualizar")
            {
               
                if (x.txt_Id.Text != "" && x.txt_Nombre.Text != "" && x.txt_Edad.Text != "" && x.txt_Direccion.Text != "")
                {
                    usuarios.Id = x.txt_Id.Text;
                    usuarios.Nombre1 = x.txt_Nombre.Text;
                    usuarios.Edad1 = Convert.ToInt32(x.txt_Edad.Text);
                    usuarios.Direccion1 = x.txt_Direccion.Text;
                   
                    opera.Actualizar(usuarios);
                    x.Dgv_usuarios.DataSource = opera.Consulta();
                    opera.Limpiar(x);

                }
            }
            else
            {
                MessageBox.Show("Hay datos vacios", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        ///Boton Agregar
        private void btn_agregar_Click(object sender, EventArgs e)
        {
            if (x.txt_Id.Text != "" && x.txt_Nombre.Text != "" && x.txt_Edad.Text != "" && x.txt_Direccion.Text != "")
            {
                usuarios.Id = x.txt_Id.Text;
                usuarios.Nombre1 = x.txt_Nombre.Text;
                usuarios.Edad1 = Convert.ToInt32(x.txt_Edad.Text);
                usuarios.Direccion1 = x.txt_Direccion.Text;
                opera.Agregar(usuarios);
                x.Dgv_usuarios.DataSource = opera.Consulta();

            }
            else
            {
                MessageBox.Show("Hay datos vacios", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        
        }
        
        ///DataGridView
        private void Dgv_usuarios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            x.txt_Id.ReadOnly = true;
            try
            {
                x.txt_Id.Text = x.Dgv_usuarios.Rows[e.RowIndex].Cells[0].Value.ToString();
                x.txt_Nombre.Text = x.Dgv_usuarios.Rows[e.RowIndex].Cells[1].Value.ToString();
                x.txt_Edad.Text = x.Dgv_usuarios.Rows[e.RowIndex].Cells[2].Value.ToString();
                x.txt_Direccion.Text = x.Dgv_usuarios.Rows[e.RowIndex].Cells[3].Value.ToString();
            }
            catch 
            {

            }
        }
        ///CAJAS DE TEXTO
        ///Caja Direccion
        private void txt_Direccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            opera.NumyLetraEspacio(e,x.txt_Direccion);
            if (e.KeyChar == (char)Keys.Enter)
            {
                x.txt_Direccion.Focus();
            }

        }
        ///Caja Edad
        private void txt_Edad_KeyPress(object sender, KeyPressEventArgs e)
        {
            opera.SoloNum(e,x.txt_Edad);
            if (e.KeyChar == (char)Keys.Enter)
            {
                x.txt_Edad.Focus();
            }
        }
        ///Caja Nombre
        private void txt_Nombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            opera.SoloLetras(e,x.txt_Nombre);
            if (e.KeyChar == (char)Keys.Enter)
            {
                x.txt_Nombre.Focus();
            }
        }
        ///Caja Caja Id
        private void txt_Id_KeyPress(object sender, KeyPressEventArgs e)
        {
            opera.SoloNum(e, x.txt_Id);
            if (e.KeyChar == (char)Keys.Enter)
            {
                x.txt_Id.Focus();
            }
        }
        /////Fin :3
    }
}
