﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EjemploMVC.Modelo;
using EjemploMVC.Modelo.DAO;

namespace EjemploMVC.Vista
{
    public partial class Form_usu : Form
    {
        DAO_Operaciones Opera = new DAO_Operaciones();
        public Form_usu()
        {
            InitializeComponent();
            Opera.conexion();
            Dgv_usuarios.DataSource = Opera.Consulta();
        }

        private void Form_usu_Load(object sender, EventArgs e)
        {

        }
    }
}
