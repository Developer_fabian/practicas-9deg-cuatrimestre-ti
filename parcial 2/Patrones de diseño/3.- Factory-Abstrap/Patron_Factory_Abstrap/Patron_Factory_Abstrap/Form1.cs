﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Patron_Factory_Abstrap
{
    public partial class Form1 : Form
    {
        Interfaz_Figura x;
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Cuadrado")
            {
                x = ClsFabrica.crear(comboBox1.Text);
                MessageBox.Show(x.mensaje());
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
