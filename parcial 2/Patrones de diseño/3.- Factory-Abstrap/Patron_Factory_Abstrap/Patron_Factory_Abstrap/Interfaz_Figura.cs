﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_Factory_Abstrap
{
    interface Interfaz_Figura
    {
        float area();
        float perimetro();
        string mensaje();

    }
}
