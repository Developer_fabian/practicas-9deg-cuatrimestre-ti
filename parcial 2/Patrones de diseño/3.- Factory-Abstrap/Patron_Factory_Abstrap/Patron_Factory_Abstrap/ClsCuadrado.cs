﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_Factory_Abstrap
{
    class ClsCuadrado:Interfaz_Figura
    {
        private float lado;
        public ClsCuadrado(int ld1)
        {
            lado = ld1;
        }
        public float perimetro()
        {
            return lado * 4;
        }

        public float area()
        {
            return lado * lado;
        }
        public string mensaje()
        {
            return "El area es: "+area()+", perimetro: "+perimetro();
        }
    }
}
