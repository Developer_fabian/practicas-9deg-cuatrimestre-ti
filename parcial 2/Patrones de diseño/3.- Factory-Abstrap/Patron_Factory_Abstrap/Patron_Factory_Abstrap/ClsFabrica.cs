﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patron_Factory_Abstrap
{
    class ClsFabrica
    {

        public static Interfaz_Figura crear(string c)
        {
            if (c == "Cuadrado")
            {
                return new ClsCuadrado(5);
            }
            else {
                return null;
            }
        }
    }
}
