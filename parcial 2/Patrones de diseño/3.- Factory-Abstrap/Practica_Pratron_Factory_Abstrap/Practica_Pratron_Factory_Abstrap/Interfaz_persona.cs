﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_Pratron_Factory_Abstrap
{
    interface Interfaz_persona
    {
        string Datos();
        float Salario();
        string Mensaje();
    }
}
