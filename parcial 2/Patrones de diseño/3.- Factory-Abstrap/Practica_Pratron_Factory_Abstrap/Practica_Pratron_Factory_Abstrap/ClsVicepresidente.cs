﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_Pratron_Factory_Abstrap
{
    class ClsVicepresidente:Interfaz_persona
    {
        private string Nombre;
        private string Apellidos;
        private float Salarios;

        public ClsVicepresidente(string nom, string ape, float salario)
        {
            Nombre = nom;
            Apellidos = ape;
            Salarios = salario;
        }

        public string Datos()
        {
            return Nombre + Apellidos;
        }

        public float Salario()
        {
            return Salarios * 4;
        }
        public string Mensaje()
        {
            return "Datos de la persona: " + Datos() + ", Salario: " + Salario().ToString();
        }
    }
}
