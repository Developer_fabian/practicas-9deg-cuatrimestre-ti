﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_Pratron_Factory_Abstrap
{
    public partial class Form1 : Form
    {
        Interfaz_persona y;
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Gerente")
            {
                y = ClsFabrica.crear(comboBox1.Text);
                MessageBox.Show(y.Mensaje());
            }
            else if (comboBox1.Text == "Presidente")
            {
                y = ClsFabrica.crear(comboBox1.Text);
                MessageBox.Show(y.Mensaje());
            }
            else if (comboBox1.Text == "Vicepresidente")
            {
                y = ClsFabrica.crear(comboBox1.Text);
                MessageBox.Show(y.Mensaje());
            }

        }
    }
}
