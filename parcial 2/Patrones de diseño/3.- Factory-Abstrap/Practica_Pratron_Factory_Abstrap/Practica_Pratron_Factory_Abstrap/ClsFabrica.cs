﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_Pratron_Factory_Abstrap
{
    class ClsFabrica
    {
        public static Interfaz_persona crear(string c)
        {
            if (c == "Gerente")
            {
                return new ClsGerente("Fabian", " Santiago", 1000);
            }
            else if (c == "Presidente")
            {
                return new ClsPresidente("Sergio", " Herrera", 1000);
            }
            else if (c == "Vicepresidente")
            {
                return new ClsVicepresidente("Martin", " Meseño", 1000);
            }
            else
            {
                return null;
            }
        }

    }
}
