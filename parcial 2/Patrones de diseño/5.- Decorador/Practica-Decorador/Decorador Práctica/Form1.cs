﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Decorador_Práctica
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Consulta campo, campo1;

        private void checkBoxNombre_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxDescripcion.Checked = false;
        }

        private void checkBoxDescripcion_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxNombre.Checked = false;
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Select select = new Select();
            string coma1 = "", coma2 = "";

            if (checkBox1.Checked)
            {
                if (checkBox2.Checked)
                {
                    coma2 = ",";
                }
                select.Clave = checkBox1.Text + coma2;
            }
            if (checkBox2.Checked)
            {
                if (checkBox1.Checked)
                {
                    coma2 = "";
                }
                select.Nombre = coma1 + checkBox2.Text + coma2;
            }
            if (checkBox3.Checked)
            {
                if (checkBox1.Checked || checkBox2.Checked)
                {
                    coma1 = ",";
                }
                select.Descripcion = coma1 + checkBox3.Text + coma2;
            }

            //*****************
            Where where = new Where();
            if (checkBoxNombre.Checked)
            {
                where.Nombre = checkBoxNombre.Text + "=like%" + txtBuscar.Text;
            }
            if (checkBoxDescripcion.Checked)
            {
                where.Descripcion = checkBoxDescripcion.Text + "=like%" + txtBuscar.Text;
            }
            if (checkBoxNombre.Checked || checkBoxDescripcion.Checked)
            {
                campo = new SelectTabla(select);
                campo1 = new WhereTabla(where);
                txtDescripcion.Text = campo.cadena() + campo1.cadena();
            }
            else
            {
                campo = new SelectTabla(select);
                txtDescripcion.Text = campo.cadena();
            }
        }
    }
}
