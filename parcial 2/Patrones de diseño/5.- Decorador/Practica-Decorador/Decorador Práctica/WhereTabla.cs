﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorador_Práctica
{
    class WhereTabla:Decorador
    {
        Consulta consulta;
        public WhereTabla(Consulta y)
        {
            consulta = y;
        }
        public override string cadena()
        {
            return consulta.cadena() + ");";
        }
    }
}
