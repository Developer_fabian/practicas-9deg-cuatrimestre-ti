﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorador_Práctica
{
    class Select : Consulta
    {
        private string clave;
        private string nombre;
        private string descripcion;

        public string Clave { get => clave; set => clave = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }

        public override string cadena()
        {
            return "select ("+clave+nombre+descripcion;
        }
    }
}
