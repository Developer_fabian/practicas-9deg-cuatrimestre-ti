﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorador_Práctica
{
    public class SelectTabla : Decorador
    {
        Consulta consulta;
        public SelectTabla(Consulta x)
        {
            consulta = x;
        }
        public override string cadena()
        {
            return consulta.cadena()+") from tblcliente";
        }
    }
}
