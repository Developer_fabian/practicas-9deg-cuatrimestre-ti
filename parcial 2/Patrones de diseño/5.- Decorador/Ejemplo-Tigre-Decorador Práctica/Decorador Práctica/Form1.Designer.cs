﻿namespace Decorador_Práctica
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbComputadora = new System.Windows.Forms.ComboBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.btnSSD = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmbComputadora
            // 
            this.cmbComputadora.FormattingEnabled = true;
            this.cmbComputadora.Items.AddRange(new object[] {
            "Escritorio",
            "Gamer",
            "Oficina"});
            this.cmbComputadora.Location = new System.Drawing.Point(46, 22);
            this.cmbComputadora.Name = "cmbComputadora";
            this.cmbComputadora.Size = new System.Drawing.Size(121, 21);
            this.cmbComputadora.TabIndex = 0;
            this.cmbComputadora.SelectedIndexChanged += new System.EventHandler(this.cmbComputadora_SelectedIndexChanged);
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(46, 167);
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(171, 82);
            this.txtDescripcion.TabIndex = 1;
            // 
            // btnSSD
            // 
            this.btnSSD.Location = new System.Drawing.Point(92, 106);
            this.btnSSD.Name = "btnSSD";
            this.btnSSD.Size = new System.Drawing.Size(75, 23);
            this.btnSSD.TabIndex = 2;
            this.btnSSD.Text = "SSD";
            this.btnSSD.UseVisualStyleBackColor = true;
            this.btnSSD.Click += new System.EventHandler(this.btnSSD_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnSSD);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.cmbComputadora);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbComputadora;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Button btnSSD;
    }
}

