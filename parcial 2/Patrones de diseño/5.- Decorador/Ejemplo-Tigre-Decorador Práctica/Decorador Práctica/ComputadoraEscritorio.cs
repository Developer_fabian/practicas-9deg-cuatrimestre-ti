﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorador_Práctica
{
    public class ComputadoraEscritorio : Computadora
    {
        public override double precio()
        {
            return 5500;
        }

        public override string descripcion()
        {
            return "Compraste una computadora de escritorio, ";
        }
    }
}
