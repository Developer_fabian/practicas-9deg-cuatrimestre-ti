﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Decorador_Práctica
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Computadora x;

        private void cmbComputadora_SelectedIndexChanged(object sender, EventArgs e)
        {
            int bandera = cmbComputadora.SelectedIndex;

            if (bandera == 0)
            {
                x = new ComputadoraEscritorio();
                txtDescripcion.Text = x.descripcion();

            }
            if (bandera==1)
            {
                x = new ComputadoraGamer();
                txtDescripcion.Text = x.descripcion();
            }
            if (bandera==2)
            {
                x = new ComputadoraOficina();
                txtDescripcion.Text = x.descripcion();
            }
        }

        private void btnSSD_Click(object sender, EventArgs e)
        {
            txtDescripcion.Text = "";
            x = new SSD(x);
            x.precio();
            txtDescripcion.Text = x.descripcion();
            
        }
    }
}
