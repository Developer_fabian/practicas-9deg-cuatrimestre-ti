﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorador_Práctica
{
    class SSD : Decorador
    {
        Computadora pc;
        public SSD(Computadora compu)
        {
            pc = compu;
        }

        public override double precio()
        {
            return pc.precio() * 1500;
        }

        public override string descripcion()
        {
            return pc.descripcion() + " tiene un costo de " + precio()+" tiene un SSD ";
        }
    }
}
