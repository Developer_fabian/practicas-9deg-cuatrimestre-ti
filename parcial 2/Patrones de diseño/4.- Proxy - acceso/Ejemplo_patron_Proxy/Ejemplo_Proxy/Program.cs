﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo_Proxy
{
    class Program
    {
        static void Main(string[] args)
        {
            ClsConductor conductor = new ClsConductor();
            conductor.Edad1 = 22;
            Interface_carro carro = new ClsCarroProxy(conductor);          
            carro.Conducir();
        }
    }
}
