﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo_Proxy
{
    class ClsCarroProxy:Interface_carro
    {
        private ClsConductor conductor;
        private ClsCarro carro;

        public ClsCarroProxy(ClsConductor conductor1)
        {
            conductor = conductor1;
        }


        public void Conducir()
        {
            if (conductor.Edad1 >= 16)
            {
                carro = new ClsCarro();
                carro.Conducir();
            }
            else
            {
                Console.WriteLine("No puede conducir");
                Console.ReadKey();
            }
        }
    }
}
